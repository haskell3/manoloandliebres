using System.Collections;
using System.Collections.Generic;
using UnityEditor.ShaderGraph.Drawing.Inspector.PropertyDrawers;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(SpriteRenderer))]
[RequireComponent(typeof(CircleCollider2D))]
public class BalaController : MonoBehaviour
{
    [SerializeField] private float atk;
    [SerializeField] private float atkDmg;
    [SerializeField] private int lvl;
    [SerializeField] private float MaxVel = 5;
    [SerializeField] private Attack.TypeDamage type;

    private Rigidbody2D m_RigidBody;
    private Vector3 m_Direction;
    private Vector3 m_target;

    private void Awake()
    {
        m_RigidBody = GetComponent<Rigidbody2D>();
    }
    public void Iniciar(Vector3 initPosition, Vector3 targetPosition, float ataque, float atkDamage, int level, Attack.TypeDamage tipo)
    {
        m_target = targetPosition;
        atk = ataque;
        atkDmg = atkDamage;
        lvl = level;
        type = tipo;
        gameObject.SetActive(true);
        m_Direction = ((Vector3)(Pathfinding.WorldToCell(targetPosition) - Pathfinding.WorldToCell(initPosition))).normalized;
        if (Mathf.Abs((int)m_Direction.x) == 1 || Mathf.Abs((int)m_Direction.y) == 1)
            transform.localPosition = (m_Direction);
        else
        {
            if (m_Direction.x < 0 && m_Direction.y < 0)
                transform.localPosition = new Vector3Int(-1, -1, 0);
            else if (m_Direction.x > 0 && m_Direction.y > 0)
                transform.localPosition = new Vector3Int(1, 1, 0);
            else if (m_Direction.x < 0)
                transform.localPosition = new Vector3Int(-1, 1, 0);
            else
                transform.localPosition = new Vector3Int(1, -1, 0);
        }
        m_Direction = (targetPosition - transform.position).normalized;
        StartCoroutine(CogerVelocidad());
        StartCoroutine(ComprobarBala());
    }

    private IEnumerator CogerVelocidad()
    {
        while (m_RigidBody.velocity.magnitude < MaxVel) 
        {
            m_RigidBody.AddForce(m_Direction * 100);
            yield return new WaitForSeconds(0.1f);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!collision.gameObject.TryGetComponent<HealthController>(out HealthController healtController))
            return;

        healtController.Damage(atk, atkDmg, lvl, type);
        gameObject.SetActive(false);
    }
    private IEnumerator ComprobarBala()
    {
        while (true)
        {
            //Wait for seconds due to some specific scenarios where transform.position == m_target is true but theres an enemy there
            yield return new WaitForSeconds(1);
            if (transform.position.x > Mathf.Abs(9) || transform.position.y > Mathf.Abs(5) || transform.position == m_target)
            {
                StopAllCoroutines();
                gameObject.SetActive(false);
            }
            
        }
    }
}
