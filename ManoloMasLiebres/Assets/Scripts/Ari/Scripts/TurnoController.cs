using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TurnoController : MonoBehaviour
{
    private bool TurnPlayer;

    private int m_PlayerCounter = 0;
    private int m_PlayerCounterCurrentTurn = 0;
    private int m_EnemyCounter = 0;
    private int m_EnemyCounterCurrentTurn = 0;

    [SerializeField] private GameEvent m_GameEventIniciCombat;
    [SerializeField] private GameEvent m_GameEventFiCombat;
    [SerializeField] private GameEvent m_GameEventEneComenzar;
    [SerializeField] private GameEvent m_GameEventPlaComenzar;

    public void IniciCombat()
    {
        TornPlayer();
    }
    
    private void TornPlayer()
    {
        Debug.Log("Turno player");
        m_GameEventPlaComenzar.Raise();
    }

    public void FiTornPlayer()
    {
        Debug.Log("Adios player");
        m_PlayerCounter--;
        Debug.Log(m_PlayerCounter);
        if (m_PlayerCounter <= 0)
        {
            m_PlayerCounter = 0;
            comprobarFiCombat(TornEnemic);
        }
    }

    private void TornEnemic()
    {
        Debug.Log("Turno enemy");
        m_GameEventEneComenzar.Raise();
    }

    public void FiTornEnemic()
    {
        Debug.Log("Adios enemy");
        m_EnemyCounter--;
        Debug.Log(m_EnemyCounter);
        if (m_EnemyCounter <= 0) 
        {
            m_EnemyCounter = 0;
            comprobarFiCombat(TornPlayer);
        }
    }

    public void OnPlayerEnterCombat()
    {
        Debug.Log("Hola player");
        m_PlayerCounter++;
        Debug.Log(m_PlayerCounter);
        m_PlayerCounterCurrentTurn = m_PlayerCounter;
    }

    public void OnEnemiesEnterCombat()
    {
        Debug.Log("Hola enemy");
        
        m_EnemyCounter++;
        Debug.Log(m_EnemyCounter);
        m_EnemyCounterCurrentTurn = m_EnemyCounter;
    }


    

    private void comprobarFiCombat(Action nextStep)
    {
        Debug.Log("Se acabo?");
        if (!(FindObjectsOfType<PlayerController>().Length == 0 || FindObjectsOfType<EnemyController>().Length == 0))
        {
            Debug.Log("No se acabo");
            nextStep();
        }
        else
        {
            Debug.Log("Efectivamente");
            m_GameEventFiCombat.Raise();
        }
    }
}
