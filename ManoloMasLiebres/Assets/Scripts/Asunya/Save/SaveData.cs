using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.Tilemaps;
using static SaveData;

public class SaveData
{
    [Serializable]
    public struct PlayerData
    {
        public int m_Movements;
        public float m_Attack;
        public float m_Magic;
        public float m_Defense;
        public float m_MagicDefense;
        public float m_MaxHealthPoints;
        public float m_HealthPoints;
        public string m_Player;
        public string m_Currentroom;

        public PlayerData(float atk, float magic, float defense, float magicdefense, float maxhp, float hp, int movements, string player, string currentroom)
        {
            m_Movements = movements;
            m_Attack = atk;
            m_Magic = magic;
            m_Defense = defense;
            m_MagicDefense = magicdefense;
            m_MaxHealthPoints = maxhp;
            m_HealthPoints = hp;
            m_Player = player;
            m_Currentroom = currentroom;
        }
    }

    public PlayerData[] m_Players;

    public void PopulateData(ISaveableObject[] _playersData)
    {
        m_Players = new PlayerData[_playersData.Length];
        for(int i = 0; i <  _playersData.Length; i++)
            m_Players[i] = _playersData[i].Save();
    }
}

public interface ISaveableObject
{
    public PlayerData Save();
    public void Load(PlayerData _playerData);
}
