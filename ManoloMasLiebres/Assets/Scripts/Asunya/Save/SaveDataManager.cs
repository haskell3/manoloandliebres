using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.InputSystem;
using UnityEngine.Tilemaps;

//[RequireComponent (typeof(PlayerControllerSave))]
public class SaveDataManager : MonoBehaviour
{
    private const string saveFileName = "savegame.json";
    private static GameObject[] players;

    [SerializeField]
    private GameObject[] playersNS;

    private void Awake()
    {
        players = playersNS;
    }

    public static void SaveData(InputAction.CallbackContext context)
    {
        ISaveableObject[] saveableObjects = FindObjectsByType<PlayerController>(FindObjectsSortMode.None);
        SaveData data = new SaveData();
        data.PopulateData(saveableObjects);
        string jsonData = JsonUtility.ToJson(data);

        try
        {
            Debug.Log("Saving: ");
            Debug.Log(jsonData);

            File.WriteAllText(saveFileName, jsonData);
        }
        catch (Exception e)
        {
            Debug.LogError($"Error while trying to save {Path.Combine(Application.persistentDataPath, saveFileName)} with exception {e}");
        }
    }

    public static void LoadData(InputAction.CallbackContext context)
    {
        Debug.Log("Trying to load");
        try
        {
            string jsonData = File.ReadAllText(saveFileName);

            SaveData data = new SaveData();
            JsonUtility.FromJsonOverwrite(jsonData, data);
            for (int i = 0; i < players.Length; i++)
            {
                players[i].GetComponent<PlayerController>().FromSaveData(data);
            }

            Tilemap[] lista = FindObjectsByType<Tilemap>(FindObjectsInactive.Include, FindObjectsSortMode.None);
            String savedData = FromSaveData(data);
            Debug.Log("Name Room Data: " + savedData);
            foreach (Tilemap room in lista)
            {
                Debug.Log("Room Name: " + room.gameObject.name);
                if (room.gameObject.name == savedData)
                {
                    Debug.Log("Correct name: " + room.gameObject.name + " is " + savedData);
                    FindObjectOfType<RoomManager>().LoadRoom(room.gameObject);
                }
            }
            Debug.Log("Load completed");
            
        }
        catch (Exception e)
        {
            Debug.LogError($"Error while trying to load {Path.Combine(Application.persistentDataPath, saveFileName)} with exception {e}");
        }
    }

    public static string FromSaveData(SaveData data)
    {
        foreach (SaveData.PlayerData circleData in data.m_Players)
        {
            return circleData.m_Currentroom;
        }
        return null;
    }
}
