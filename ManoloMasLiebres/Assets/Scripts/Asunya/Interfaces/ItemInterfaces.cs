using System;
using UnityEngine;


public interface IItem
{
    public string Name { get; set; }
    public Sprite Sprite { get; set; }

    public bool UsedBy(GameObject go);
}
