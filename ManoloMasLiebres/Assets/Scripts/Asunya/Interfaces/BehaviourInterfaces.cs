using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IHealable
{
    public void Heal(float healAmount);
}

public interface IDamageable
{
    public void Damage(float damageAmount, float atkDmg, int level, Attack.TypeDamage type);
}

public interface IAttack
{
    public void AttackPotion(float attackAmount);
}

public interface Morible
{
    public void Die();
}
