using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class StatsUIController : MonoBehaviour
{
    private TextMeshProUGUI stats;

    [SerializeField]
    private PlayerController player;
    void Awake()
    {
        stats = GetComponent<TextMeshProUGUI>();
        mostrar();
    }

    public void mostrar()
    {
        stats.text = "ATK: " + player.mirarAttack() + "\r\nMAG: " + player.mirarMagic() + "\r\nDEF: " + player.mirarDefense() + "\r\nMAG.DEF: " + player.mirarMagicDefense() + "\r\nMOV: " + player.mirarMovement() + "\r\nHP: " + player.mirarHp() + "/" + player.mirarHpMax();
    }
}
