using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class DisplayAtk : MonoBehaviour
{
    /*
    [Header("Functionality")]
    [SerializeField]
    private GameEvent m_Event;
    */
    [Header("Display")]
    [SerializeField]
    private TextMeshProUGUI m_Text;
    [SerializeField]
    private TextMeshProUGUI m_StatsText;

    public void Load(Attack attack)
    {
        m_Text.text = attack.name;
        if (m_StatsText != null)
        {
            m_StatsText.text = "Ataque: " + attack.Damage + "\r\nTipo: " + attack.TypeDmg + "\r\nEffecto: " + attack.Effect + "\r\nRange: " + attack.Range + "\r\nArea: " + attack.Area + "\r\nCooldown: " + attack.Cooldown;
        }
    }

    public void seleccionado()
    {
        Debug.Log(m_Text.text + ": seleccionado");
        Color newColor = m_Text.color;
        newColor.a = 1f;
        m_Text.color = newColor;
    }

    public void deseleccionado()
    {
        Color newColor = m_Text.color;
        newColor.a = 0.40f;
        m_Text.color = newColor;
    }


}
