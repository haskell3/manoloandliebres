using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VidaUIController : MonoBehaviour
{
    private Image barraVida;

    [SerializeField]
    private PlayerController hp;

    void Awake()
    {
        barraVida = GetComponent<Image>();
    }
    private void Start()
    {
        mostrar();
    }

    public void mostrar()
    {
        if(barraVida != null)
            barraVida.fillAmount = hp.mirarHp() / hp.mirarHpMax();
    }
}
