using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class XpUIController : MonoBehaviour
{
    private Image barraXp;

    [SerializeField]
    private PlayerController xp;

    void Start()
    {
        barraXp = GetComponent<Image>();
        mostrar();
    }

    public void mostrar()
    {
        barraXp.fillAmount = xp.mirarXp() / xp.mirarXpMax();
    }
}
