using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisplayLisAtk : MonoBehaviour
{
    
    [SerializeField]
    private Players m_Player;
    [SerializeField]
    private GameObject m_DisplayAtkParent;
    [SerializeField]
    private GameObject m_DisplayAtkPrefab;
    [SerializeField]
    private GameObject m_AimObject;
    private Action OnDeseleccionar;

    private void Start()
    {
        ClearDisplay();
        FillDisplay();
    }

    private void FillDisplay()
    {
        foreach (Attack atk in m_Player.AttackList)
        {
            GameObject displayedItem = Instantiate(m_DisplayAtkPrefab, m_DisplayAtkParent.transform);
            displayedItem.GetComponent<DisplayAtk>().Load(atk);
            OnDeseleccionar += displayedItem.GetComponent<DisplayAtk>().deseleccionado;
        }
    }

    private void ClearDisplay()
    {
        foreach (Transform child in m_DisplayAtkParent.transform)
            Destroy(child.gameObject);
    }

    public void seleccionado(int Atk)
    {
        this.gameObject.SetActive(true);
        if (Atk >= m_DisplayAtkParent.transform.childCount) return;
        OnDeseleccionar();
        m_AimObject = m_DisplayAtkParent.transform.GetChild(Atk).gameObject;
        m_AimObject.GetComponent<DisplayAtk>().seleccionado();
    }

    public void mostrar()
    {
        if (m_DisplayAtkParent != null)
        {
            if (m_DisplayAtkParent.activeInHierarchy)
            {
                m_DisplayAtkParent.SetActive(false);
            }
            else
            {
                m_DisplayAtkParent.SetActive(true);
            }
        }
    }

}
