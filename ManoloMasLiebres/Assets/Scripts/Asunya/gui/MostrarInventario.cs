using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MostrarInventario : MonoBehaviour
{
    [SerializeField]
    private GameObject inventory;
    [SerializeField]
    private GameEvent stats;
    [SerializeField]
    private GameEvent refreshInvetory;

    public void mostrar()
    {
      if (inventory != null)
        {
            
            if (inventory.activeInHierarchy)
            {
                inventory.SetActive(false);
            }else {
                inventory.SetActive(true);
                stats?.Raise();
                refreshInvetory?.Raise();
            }
        }
    }
}
