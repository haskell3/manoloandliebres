using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using static UnityEditor.Progress;


public class DisplayItem : MonoBehaviour
{
    [Header("Functionality")]
    [SerializeField]
    private GameEventItem m_Event;

    [Header("Display")]
    [SerializeField]
    private TextMeshProUGUI m_Text;
    [SerializeField]
    private TextMeshProUGUI m_AmountText;
    [SerializeField]
    private Image m_Image;

    public void Load(Item item)
    {
        m_Text.text = item.Name;
        m_Image.sprite = item.Sprite;
        GetComponent<Button>().onClick.RemoveAllListeners();
        GetComponent<Button>().onClick.AddListener(() => RaiseEvent(item));
    }

    public void Load(Backpack.ItemSlot itemSlot)
    {
        Load(itemSlot.Item);
        m_AmountText.text = itemSlot.Amount.ToString();
    }

    private void RaiseEvent(Item item)
    {
        m_Event.Raise(item);
    }
}
