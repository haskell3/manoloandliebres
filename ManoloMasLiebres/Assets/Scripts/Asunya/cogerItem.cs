using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor.U2D.Aseprite;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.InputSystem;
using UnityEngine.Tilemaps;

[RequireComponent(typeof(HealthController))]
[RequireComponent(typeof(ObjectPool))]
public class cogerItem : MonoBehaviour, ISaveableObject, IAttack
{

    [SerializeField] private Players m_Player;

    private enum PlayerType { WIZARD, WARRIOR }
    [SerializeField] private PlayerType m_PlayerType;
    private enum PlayerNumber { PLAYER1, PLAYER2 }
    [SerializeField] private PlayerNumber m_PlayerNumber;
    private enum SwitchMachineStates { NONE, FIGHTING, WAITING, EXPLORING, CHOOSING, AIMING };
    [SerializeField] private SwitchMachineStates m_CurrentState;
    private enum SwitchActionMaps { MOVIMIENTOTURNO, SELECCIONARATAQUE, DIRIGIRATAQUE, TURNOESPERA, FINALTURNO };
    [SerializeField] private SwitchActionMaps m_CurrentActionMapEnum;

    private Vector3Int currentCell;
    [SerializeField] Tilemap m_Tiles;

    private Vector2 input;
    private Vector2 LastInput;

    [Tooltip("The stats of the player, they are initiated from the SO")]
    [SerializeField] private int m_Movements;
    [SerializeField] private int m_MaxMovements;
    [SerializeField] private float m_Attack;
    [SerializeField] private float m_Magic;
    [SerializeField] private float m_Defense;
    [SerializeField] private float m_MagicDefense;
    [SerializeField] private float m_MaxHealthPoints;
    [SerializeField] private float m_HealthPoints;
    [SerializeField] private Attack[] m_AttackList;
    [SerializeField] private Attack m_CurrentAttack;
    private Sprite m_Sprite;

    [Tooltip("To check if the player can move or attack")]
    [SerializeField] private bool m_CanMove;
    [SerializeField] private bool m_CanAttack;

    [SerializeField] private GameEvent m_GameEventMeApunto;
    [SerializeField] private GameEvent m_GameEventAcabeMiTurno;
    [SerializeField] private GameEvent refreshInvetory;
    [SerializeField] private GameEvent m_listAtk;
    [SerializeField] private GameEventInt m_SelectAttack;


    private GameObject m_AimObject;
    private HealthController m_HealthController;
    private ObjectPool m_ObjectPool;

    private readonly float initX = -7.5f;
    private readonly float initY = 2.5f;

    private int m_NumberAttack;

    private void ChangeStateChoosing(InputAction.CallbackContext context)
    {
        ChangeState(SwitchMachineStates.CHOOSING);
    }
    private void ChangeStateAiming(InputAction.CallbackContext context)
    {
        ChangeState(SwitchMachineStates.AIMING);
    }
    private void ChangeStateFighting(InputAction.CallbackContext context)
    {
        ChangeState(SwitchMachineStates.FIGHTING);
    }
    private void ChangeStateWaiting(InputAction.CallbackContext context)
    {
        ChangeState(SwitchMachineStates.CHOOSING);
    }
    private void ChangeStateExploring(InputAction.CallbackContext context)
    {
        ChangeState(SwitchMachineStates.EXPLORING);
    }

    public void ChangeStateExploring()
    {
        ChangeState(SwitchMachineStates.EXPLORING);
    }

    private void ChangeState(SwitchMachineStates newState)
    {
        if (newState == m_CurrentState)
            return;

        ExitState();
        InitState(newState);
        Debug.Log(m_CurrentState);
    }

    private void InitState(SwitchMachineStates currentState)
    {
        m_CurrentState = currentState;
        switch (m_CurrentState)
        {
            case SwitchMachineStates.FIGHTING:
                if (m_Movements > 0)
                    m_CanMove = true;
                ChangeActionMap(SwitchActionMaps.MOVIMIENTOTURNO);
                break;

            case SwitchMachineStates.WAITING:
                m_CanMove = false;
                ChangeActionMap(SwitchActionMaps.TURNOESPERA);
                break;

            case SwitchMachineStates.EXPLORING:
                m_CanMove = true;
                ChangeActionMap(SwitchActionMaps.FINALTURNO);
                break;
            case SwitchMachineStates.CHOOSING:
                m_CanMove = false;
                m_listAtk?.Raise();
                m_SelectAttack.Raise(m_NumberAttack);
                ChangeActionMap(SwitchActionMaps.SELECCIONARATAQUE);
                break;

            case SwitchMachineStates.AIMING:
                mirarCooldown();
                m_CanMove = false;
                m_AimObject.gameObject.SetActive(true);
                ChangeActionMap(SwitchActionMaps.DIRIGIRATAQUE);
                break;

            default:
                break;
        }
    }
    private void UpdateState()
    {

        switch (m_CurrentState)
        {
            case SwitchMachineStates.FIGHTING:
                if (!m_CanAttack && !m_CanMove)
                    EndTurn();
                break;

            case SwitchMachineStates.WAITING:

                break;

            case SwitchMachineStates.EXPLORING:

                break;
            case SwitchMachineStates.CHOOSING:

                break;

            case SwitchMachineStates.AIMING:

                break;
            default:
                break;
        }
    }

    private void ExitState()
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.FIGHTING:
                m_CanMove = false;
                break;

            case SwitchMachineStates.WAITING:
                m_CanMove = false;
                break;

            case SwitchMachineStates.EXPLORING:
                m_CanMove = false;
                break;
            case SwitchMachineStates.CHOOSING:
                m_CanMove = false;
                m_CurrentAttack = m_AttackList[m_NumberAttack];
                m_listAtk?.Raise();
                break;

            case SwitchMachineStates.AIMING:
                m_CanMove = false;
                m_AimObject.transform.localPosition = Vector3.zero;
                m_AimObject.gameObject.SetActive(false);
                break;
            default:
                break;
        }
    }

    [SerializeField]
    private InputActionAsset m_InputAsset;
    private InputActionAsset m_Input;
    private InputAction m_MovementAction;
    private InputActionMap m_CurrentActionMap;


    void Awake()
    {
        m_HealthController = GetComponent<HealthController>();
        m_ObjectPool = GetComponent<ObjectPool>();
        m_AimObject = transform.GetChild(0).gameObject;
        m_Movements = m_MaxMovements;
        m_CanAttack = true;

        Assert.IsNotNull(m_InputAsset);

        m_Input = Instantiate(m_InputAsset);
        ChooseBindingMask();
        m_CurrentActionMap = m_Input.FindActionMap("MovimientoTurno");
        m_MovementAction = m_CurrentActionMap.FindAction("Movement");

        currentCell = m_Tiles.WorldToCell(transform.position);
        InitInicial();
        m_NumberAttack = 0;
    }

    public float mirarAttack()
    {
        return m_Attack;
    }

    public float mirarMagic()
    {
        return m_Magic;
    }

    public float mirarDefense()
    {
        return m_Defense;
    }

    public float mirarMagicDefense()
    {
        return m_MagicDefense;
    }

    public int mirarMovement()
    {
        return m_Movements;
    }

    public float mirarHpMax()
    {
        return m_Player.HealthPointsMax;
    }

    public float mirarHp()
    {
        return m_HealthController.mirarHP();
    }


    private void ChooseBindingMask()
    {
        switch (m_PlayerNumber)
        {
            case PlayerNumber.PLAYER1:
                m_Input.bindingMask = InputBinding.MaskByGroup("Player1");
                break;
            case PlayerNumber.PLAYER2:
                m_Input.bindingMask = InputBinding.MaskByGroup("Player2");
                break;
        }
    }

    private void Move(InputAction.CallbackContext context)
    {
        input = m_MovementAction.ReadValue<Vector2>();
        //Check if the input isn't diagonal
        if (!(Mathf.Abs(input.x) == 1 || Mathf.Abs(input.y) == 1))
            return;
        LastInput = input;
        //Check if the player can move and the new cell is walkable
        if (!(m_CanMove && Pathfinding.IsWalkableTile(currentCell + new Vector3Int((int)input.x, (int)input.y, 0), m_Tiles)))
            return;
        Vector3 raycastorigin = m_Tiles.GetCellCenterWorld(currentCell + new Vector3Int((int)input.x, (int)input.y, 0));
        raycastorigin.z = -10;
        RaycastHit2D hit = Physics2D.Raycast(raycastorigin, Vector3.forward, 30);
        //Check if the new cell is being used
        if (hit.rigidbody == null)
        {
            transform.position = m_Tiles.GetCellCenterWorld(currentCell + new Vector3Int((int)input.x, (int)input.y, 0));
            currentCell = m_Tiles.WorldToCell(transform.position);

            if (m_CurrentState == SwitchMachineStates.FIGHTING)
            {
                m_Movements--;
                if (m_Movements <= 0)
                {
                    m_CanMove = false;
                }
            }
        }
    }
    private void MoveAim(InputAction.CallbackContext context)
    {
        input = m_MovementAction.ReadValue<Vector2>();
        //Check if the input isn't diagonal
        if ((Mathf.Abs(input.x) == 1 || Mathf.Abs(input.y) == 1))
        {
            if (Pathfinding.InManhattanDistance(m_Tiles.WorldToCell(m_AimObject.transform.position) + new Vector3Int((int)input.x, (int)input.y, 0), Pathfinding.WorldToCell(transform.position), m_CurrentAttack.Range))
                m_AimObject.transform.position = m_Tiles.GetCellCenterWorld(m_Tiles.WorldToCell(m_AimObject.transform.position) + new Vector3Int((int)input.x, (int)input.y, 0));
        }
    }
    private void SelectAttack(InputAction.CallbackContext context)
    {
        input = m_MovementAction.ReadValue<Vector2>();
        if (input.y > 0)
        {
            if (m_NumberAttack == 0)
            {
                m_NumberAttack = 4;
            }
            m_NumberAttack--;
            m_SelectAttack.Raise(m_NumberAttack);
        }
        else if (input.y < 0)
        {
            if (m_NumberAttack == 3)
            {
                m_NumberAttack = -1;
            }
            m_NumberAttack++;
            m_SelectAttack.Raise(m_NumberAttack);
        }
        Debug.Log(m_NumberAttack);
    }

    private void mirarCooldown()
    {
        if (m_CurrentAttack.TurnosCooldown > 0)
        {
            ChangeState(SwitchMachineStates.CHOOSING);
        }
    }

    private void ponerCooldown()
    {
        m_CurrentAttack.TurnosCooldown = m_CurrentAttack.Cooldown;
    }

    private void restarCooldown()
    {
        foreach(Attack a in m_AttackList)
        {
            if(a != m_CurrentAttack)
            {
                a.TurnosCooldown -= 1;
            }
        }
    }

    private void Attack(InputAction.CallbackContext context)
    {
        m_CanAttack = false;

        if (m_CurrentAttack.TypeAtk == global::Attack.TypeAttack.Pooled)
        {
            GameObject obj = m_ObjectPool.GetPooledObject();
            if (obj?.GetComponent<BalaController>() != null)
            {
                if (m_CurrentAttack.TypeDmg == global::Attack.TypeDamage.ATTACK)
                    obj.GetComponent<BalaController>().Iniciar(transform.position, m_AimObject.transform.position, m_Attack, m_CurrentAttack.Damage, 50, global::Attack.TypeDamage.ATTACK);
                else
                    obj.GetComponent<BalaController>().Iniciar(transform.position, m_AimObject.transform.position, m_Magic, m_CurrentAttack.Damage, 50, global::Attack.TypeDamage.MAGIC);
            }
        }
        else if (m_CurrentAttack.TypeAtk == global::Attack.TypeAttack.Impulsive)
        {
            Vector3Int pos = m_Tiles.WorldToCell(m_AimObject.transform.position);
            RangeImpulsiveDamage(pos, m_CurrentAttack.Area);
        }
        else
        {
            Vector3 raycastorigin = m_AimObject.transform.position;
            raycastorigin.z = -10;
            RaycastHit2D hit = Physics2D.Raycast(raycastorigin, Vector3.forward, 30);

            if (hit.rigidbody?.gameObject.GetComponent<HealthController>() != null)
            {
                if (m_CurrentAttack.TypeDmg == global::Attack.TypeDamage.ATTACK)
                    hit.rigidbody.gameObject.GetComponent<HealthController>().Damage(m_Attack, m_CurrentAttack.Damage, 50, global::Attack.TypeDamage.ATTACK);
                else
                    hit.rigidbody.gameObject.GetComponent<HealthController>().Damage(m_Magic, m_CurrentAttack.Damage, 50, global::Attack.TypeDamage.MAGIC);

            }
        }
        ponerCooldown();
        ChangeState(SwitchMachineStates.FIGHTING);
    }

    private void Start()
    {
        ChangeState(SwitchMachineStates.EXPLORING);
    }

    private void Update()
    {
        UpdateState();
        //if (Input.GetKeyDown(KeyCode.S)) ChangeState(SwitchMachineStates.FIGHTING);

    }
    private void EndTurn()
    {
        ChangeState(SwitchMachineStates.WAITING);
        AttackPotionEnd();
        restarCooldown();
        m_GameEventAcabeMiTurno.Raise();
    }
    private void ChangeActionMap(SwitchActionMaps newMap)
    {

        if (newMap == m_CurrentActionMapEnum)
            return;

        ExitActionMap();
        InitActionMap(newMap);
    }

    private void InitActionMap(SwitchActionMaps currentMap)
    {
        m_CurrentActionMapEnum = currentMap;
        switch (m_CurrentActionMapEnum)
        {
            case SwitchActionMaps.MOVIMIENTOTURNO:
                m_CurrentActionMap = m_Input.FindActionMap("MovimientoTurno");
                m_MovementAction = m_CurrentActionMap.FindAction("Movement");
                m_MovementAction.performed += Move;
                if (m_CanAttack)
                    m_CurrentActionMap.FindAction("Aceptar").performed += ChangeStateChoosing;
                break;

            case SwitchActionMaps.SELECCIONARATAQUE:
                m_CurrentActionMap = m_Input.FindActionMap("SeleccionarAtaque");
                m_MovementAction = m_CurrentActionMap.FindAction("Movement");
                m_MovementAction.performed += SelectAttack;
                m_CurrentActionMap.FindAction("Atras").performed += ChangeStateFighting;
                m_CurrentActionMap.FindAction("Aceptar").performed += ChangeStateAiming;
                break;

            case SwitchActionMaps.DIRIGIRATAQUE:
                m_CurrentActionMap = m_Input.FindActionMap("DirigirAtaque");
                m_MovementAction = m_CurrentActionMap.FindAction("Movement");
                m_MovementAction.performed += MoveAim;
                m_CurrentActionMap.FindAction("Atras").performed += ChangeStateChoosing;
                m_CurrentActionMap.FindAction("Aceptar").performed += Attack;
                break;
            case SwitchActionMaps.TURNOESPERA:
                m_CurrentActionMap = m_Input.FindActionMap("TurnoEspera");
                break;

            case SwitchActionMaps.FINALTURNO:
                m_CurrentActionMap = m_Input.FindActionMap("FinalTurno");
                m_MovementAction = m_CurrentActionMap.FindAction("Movement");
                m_MovementAction.performed += Move;
                m_CurrentActionMap.FindAction("Aceptar").performed += TakeObject;
                m_CurrentActionMap.FindAction("Cargar").performed += SaveDataManager.LoadData;
                m_CurrentActionMap.FindAction("Guardar").performed += SaveDataManager.SaveData;

                break;

            default:
                break;
        }
        m_CurrentActionMap.Enable();
    }

    private void ExitActionMap()
    {
        switch (m_CurrentActionMapEnum)
        {
            case SwitchActionMaps.MOVIMIENTOTURNO:
                m_MovementAction.performed -= Move;
                m_CurrentActionMap.FindAction("Aceptar").performed -= ChangeStateChoosing;
                break;

            case SwitchActionMaps.SELECCIONARATAQUE:
                m_MovementAction.performed -= SelectAttack;
                m_CurrentActionMap.FindAction("Aceptar").performed -= ChangeStateAiming;
                m_CurrentActionMap.FindAction("Atras").performed -= ChangeStateFighting;
                break;

            case SwitchActionMaps.DIRIGIRATAQUE:
                m_CurrentActionMap.FindAction("Atras").performed -= ChangeStateChoosing;
                m_CurrentActionMap.FindAction("Aceptar").performed -= Attack;
                m_MovementAction.performed -= MoveAim;
                break;
            case SwitchActionMaps.TURNOESPERA:

                break;

            case SwitchActionMaps.FINALTURNO:
                m_MovementAction.performed -= Move;
                m_CurrentActionMap.FindAction("Aceptar").performed -= TakeObject;
                break;

            default:
                break;
        }
        m_CurrentActionMap.Disable();
    }
    public void MyTurn()
    {

        m_GameEventMeApunto.Raise();
        m_Movements = m_MaxMovements;
        m_CanAttack = true;
        ChangeState(SwitchMachineStates.FIGHTING);
    }
    public void EnemyTurn()
    {
        ChangeState(SwitchMachineStates.WAITING);
    }
    public void TakeObject(InputAction.CallbackContext context)
    {
        GameObject objecte = Pathfinding.MirarCelda(currentCell + new Vector3(LastInput.x, LastInput.y, -10));
        if (objecte != null)
        {

            if (objecte.TryGetComponent<ItemWorld>(out ItemWorld item))
            {
                m_Player.Backpack.AddItem(item.item);
                refreshInvetory?.Raise();
                objecte.SetActive(false);
            }
            else
            {
                if (objecte.GetComponent<EquipWorld>().equip.TypeEquip.Equals(Equipable.TypeEquipable.WEAPON))
                {
                    Equipable weapon = m_Player.Weapon;
                    m_Player.Weapon = objecte.GetComponent<EquipWorld>().equip;
                    objecte.GetComponent<EquipWorld>().equip = weapon;
                    objecte.SetActive(false);
                }
                else
                {
                    Equipable armor = m_Player.Armor;
                    m_Player.Armor = objecte.GetComponent<EquipWorld>().equip;
                    objecte.GetComponent<EquipWorld>().equip = armor;
                    objecte.SetActive(false);
                }
                    
            }
            
        }
    }

    public void InitInicial()
    {
        m_MaxMovements = m_Player.Movement;
        m_Movements = m_MaxMovements;
        m_AttackList = m_Player.AttackList;
        m_Attack = m_Player.Attack;
        m_Magic = m_Player.Magic;
        m_Defense = m_Player.Defense;
        m_MagicDefense = m_Player.Magicdefense;
        m_MaxHealthPoints = m_Player.HealthPointsMax;
        m_HealthPoints = m_Player.HealthPoints;
        gameObject.GetComponent<SpriteRenderer>().sprite = m_Player.SpritE;
        m_HealthController.incializarDefensa(m_Defense);
        m_HealthController.incializarDefensaMagica(m_MagicDefense);
        m_HealthController.incializarHP(m_HealthPoints);
    }

    public void CambioDeSala()
    {
        m_Tiles = RoomManager.currentRoom;
        if (m_PlayerNumber == PlayerNumber.PLAYER1)
            transform.position = new Vector3(initX, initY, 0);
        else
            transform.position = new Vector3(initX, -initY, 0);
        currentCell = m_Tiles.WorldToCell(transform.position);
    }

    public void RangeImpulsiveDamage(Vector3Int initposition, int range)
    {
        List<GameObject> objects = new List<GameObject>();
        RoomManager.SelectObjectsInRangedAttack(initposition, range, m_Tiles, out objects);
        for (int i = 0; i < objects.Count; i++)
        {
            Vector3Int dir = Vector3Int.zero;
            if (initposition != Vector3Int.zero)
            {

                Vector3Int posene = m_Tiles.WorldToCell(objects[i].transform.position);
                //int distabsx = Mathf.Abs(posene.x - initposition.x);
                //int distabsy = Mathf.Abs(posene.y - initposition.y);
                int distx = posene.x - initposition.x;
                int disty = posene.y - initposition.y;

                if (distx == 0)
                {
                    if (disty > 0) dir += Vector3Int.up;
                    else dir += Vector3Int.down;
                }
                else if (disty == 0)
                {
                    if (distx > 0) dir += Vector3Int.right;
                    else dir += Vector3Int.left;
                }
                else
                {
                    if (distx > 0) dir += Vector3Int.right;
                    else dir += Vector3Int.left;

                    if (disty > 0) dir += Vector3Int.up;
                    else dir += Vector3Int.down;
                }

            }
            Vector3 newpos = objects[i].transform.position + dir;
            Vector3Int newposcell = m_Tiles.WorldToCell(newpos);
            if (Pathfinding.IsWalkableTile(newposcell, m_Tiles) && !RoomManager.IsOccupied(newposcell))
            {
                objects[i].transform.position += dir;
            }


            if (objects[i].GetComponent<HealthController>() != null)
            {
                if (m_CurrentAttack.TypeDmg == global::Attack.TypeDamage.ATTACK)
                    objects[i].GetComponent<HealthController>().Damage(m_Attack, m_CurrentAttack.Damage, 50, global::Attack.TypeDamage.ATTACK);
                else
                    objects[i].GetComponent<HealthController>().Damage(m_Magic, m_CurrentAttack.Damage, 50, global::Attack.TypeDamage.MAGIC);

            }

        }
    }

    public void AttackPotion(float attackAmount)
    {
        m_Attack += attackAmount;
        Debug.Log(string.Format("Damage augment by {0} points. Damage: {1}", attackAmount, m_Player.Attack));
    }

    public void AttackPotionEnd()
    {
        m_Attack = m_Player.Attack;
    }

    public SaveData.PlayerData Save()
    {
        return new SaveData.PlayerData(m_Attack, m_Magic, m_Defense, m_MagicDefense, mirarHpMax(), mirarHp(), m_Movements, m_Player.name, RoomManager.currentRoom.gameObject.name);
    }

    public void Load(SaveData.PlayerData _playerData)
    {
        if (_playerData.m_Player == m_Player.name)
        {
            m_Movements = _playerData.m_Movements;
            m_Attack = _playerData.m_Attack;
            m_Magic = _playerData.m_Magic;
            m_Defense = _playerData.m_Defense;
            m_MagicDefense = _playerData.m_MagicDefense;
            m_HealthPoints = _playerData.m_HealthPoints;
            m_MaxHealthPoints = _playerData.m_MaxHealthPoints;
        }
    }
    public void FromSaveData(SaveData data)
    {
        foreach (SaveData.PlayerData circleData in data.m_Players)
        {
            Load(circleData);
        }
    }
}
