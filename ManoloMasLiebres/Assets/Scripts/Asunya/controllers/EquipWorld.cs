using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using static UnityEditor.Progress;

public class EquipWorld : MonoBehaviour
{
    [SerializeField] private bool inicial = false;
    [SerializeField] public Equipable equip;
    [SerializeField] public Tilemap m_Tiles;
    private Vector3Int currentCell;
    private SpriteRenderer m_SpriteRenderer;
    private void Awake()
    {
        m_SpriteRenderer = GetComponent<SpriteRenderer>();
        if (inicial) Inicia(equip, m_Tiles, transform.position);
    }
    public void Inicia(Equipable i, Tilemap t, Vector3 pos)
    {
        equip = i;
        m_Tiles = t;
        m_SpriteRenderer.sprite = equip.SpritE;
        transform.position = pos;
        currentCell = m_Tiles.WorldToCell(transform.position);
        gameObject.SetActive(true);
    }

    public void Reinicia()
    {
        m_SpriteRenderer.sprite = equip.SpritE;
    }
}
