using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Windows;

public class HealthController : MonoBehaviour, IHealable, IDamageable
{
    [SerializeField]
    private Players player;
    [SerializeField]
    private float hp;
    [SerializeField]
    private float defense;
    [SerializeField]
    private float magicDefense;
    [SerializeField]
    private GameEvent vidaUI_Event;

    public void incializarHP(float vida)
    {
        hp = vida;
    }
    public void incializarDefensa(float def)
    {
        defense = def;
    }
    public void incializarDefensaMagica(float dM)
    {
        magicDefense = dM;
    }

    public float mirarHP()
    {
        return hp;
    }

    public float mirarDef()
    {
        return defense;
    }

    public float mirarMagDef()
    {
        return magicDefense;
    }

    //Damageamount es el ataque/magia del atacante, atkDmg es el poder del ataque, el level es el nivel del atacante y el type se esxplica solo
    public void Damage(float damageAmount, float atkDmg, int level, Attack.TypeDamage type)
    {
        float da�o; 
        if(type == Attack.TypeDamage.ATTACK)
            da�o = CalcularDa�o(damageAmount, atkDmg, level, defense);
        else
            da�o = CalcularDa�o(damageAmount, atkDmg, level, magicDefense);
        hp -= da�o;
       
        if (hp <= 0)
        {
            Muerte();   
        }
        vidaUI_Event?.Raise();
        Debug.Log(string.Format("Received {0} damage. Remaining HP: {1}", da�o, hp));
    }
    private static float CalcularDa�o(float atk, float atkDmg, int lvl, float def)
    {
        return (float)((((atk * atkDmg * (0.2 * lvl +1)) / (25 * def)) + 2));
    }

    public void Heal(float healAmount)
    {
        hp += healAmount;
        
        if (hp > player.HealthPointsMax)
            hp = player.HealthPointsMax;
        vidaUI_Event?.Raise();
        Debug.Log(string.Format("Healed by {0} points. Remaining HP: {1}", healAmount, hp));
    }
    public void ActualizarHp()
    {
        vidaUI_Event?.Raise();
    }
    public void Muerte()
    {
        hp = 0;
        gameObject.GetComponent<Morible>().Die();
        gameObject.SetActive(false);
        if (gameObject.TryGetComponent<PlayerController>(out PlayerController cont))
        {
            cont.enabled = false;
        }
    }
}
