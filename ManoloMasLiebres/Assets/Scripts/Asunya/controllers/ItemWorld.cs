using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Tilemaps;

public class ItemWorld : MonoBehaviour
{
    [SerializeField] private bool inicial = false;
    [SerializeField] public Item item;
    [SerializeField] public Tilemap m_Tiles;
    private Vector3Int currentCell;
    private SpriteRenderer m_SpriteRenderer;
    private void Awake()
    {
        m_SpriteRenderer = GetComponent<SpriteRenderer>();
        if (inicial) Inicia(item, m_Tiles, transform.position);
    }
    public void Inicia(Item i, Tilemap t, Vector3 pos)
    {
        item = i;
        m_Tiles = t;
        m_SpriteRenderer.sprite = item.Sprite;
        transform.position = pos;
        currentCell = m_Tiles.WorldToCell(transform.position);
        gameObject.SetActive(true);
    }
}
