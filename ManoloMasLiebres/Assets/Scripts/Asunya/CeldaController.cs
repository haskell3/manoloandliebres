using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class CeldaController : MonoBehaviour
{
    [SerializeField] Tilemap m_Tiles;
    [SerializeField] LayerMask layerMaskItem;
    private Vector3Int currentCell;
    
    private void Awake()
    {
        this.transform.position = m_Tiles.GetCellCenterWorld(currentCell);
    }

    public static GameObject MirarCelda(Vector3 vector)
    {
        RaycastHit2D hit = Physics2D.Raycast(vector, Vector3.forward, Mathf.Infinity);

        if (hit.rigidbody != null)
        {
            Debug.Log("RaycastHit: " + hit.transform.name);
            return hit.rigidbody.gameObject;
        }
        return null;
    }
}
