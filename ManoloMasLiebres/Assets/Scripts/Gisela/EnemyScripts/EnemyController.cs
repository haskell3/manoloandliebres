using System.Collections;
using System.Collections.Generic;
using UnityEditor.Build.Player;
using UnityEditor.Experimental.GraphView;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.UIElements;

public class EnemyController : MonoBehaviour, Morible
{
    [SerializeField]
    Tilemap m_Tilemap;
    [SerializeField]
    LayerMask m_LayerMaskPlayer;
    [SerializeField] float timeMoving;

    Vector3Int m_Origin;
    Vector3Int m_Destiny;
    List<Vector3Int> m_Cells;

    [SerializeField] private GameEvent m_GameEventMeApunto;
    [SerializeField] private GameEvent m_GameEventAcabeMiTurno;

    private HealthController m_HealthController;

    enum EnemyState { NONE, MOVETOPLAYER, ATTACK, WAITING }
    EnemyState m_CurrentState;

    [SerializeField]
    private Enemy enemySO;
    [SerializeField]
    private Attack currentAttack;

    //Stats
    float Attack;
    float Magic;
    float Defense;
    float MagicDefense;
    int Movement;
    int MaxMovement;
    float HealthPoints;
    float HealthPointsMax;
    int lvl=50;
    Attack[] AttackList;
    Attack.Effects Effect;

    private void ChangeState(EnemyState newState)
    {

        if (newState == m_CurrentState)
            return;

        ExitState();
        InitState(newState);
    }

    private void InitState(EnemyState currentState)
    {
        m_CurrentState = currentState;
        switch (m_CurrentState)
        {
            case EnemyState.MOVETOPLAYER:
                
                if (Pathfinding.IsWalkableTile(m_Origin, m_Tilemap) && Pathfinding.IsWalkableTile(m_Destiny, m_Tilemap))
                {
                    Pathfinding.FindPath(m_Origin, m_Destiny, out m_Cells, m_Tilemap);
                }
                StartCoroutine(Move());

                break;

            case EnemyState.WAITING:
                
                break;

            case EnemyState.ATTACK:
                Vector3 raycastorigin = m_Tilemap.GetCellCenterWorld(m_Cells[m_Cells.Count - 1]);
                raycastorigin.z = -10;
                RaycastHit2D hit = Physics2D.Raycast(raycastorigin, Vector3.forward, 30, m_LayerMaskPlayer);

                if (hit.rigidbody != null)
                {
                    Debug.Log("RaycastHit :" + hit.transform.name);
                    GameObject obj = hit.rigidbody.gameObject;
                    
                    if (currentAttack.TypeDmg == global::Attack.TypeDamage.ATTACK)
                        hit.rigidbody.gameObject.GetComponent<HealthController>().Damage(Attack, currentAttack.Damage, 50, global::Attack.TypeDamage.ATTACK);
                    else
                        hit.rigidbody.gameObject.GetComponent<HealthController>().Damage(Magic, currentAttack.Damage, 50, global::Attack.TypeDamage.MAGIC);


                }

                PlayerTurn();
                break;

            default:
                break;
        }
    }
    private void UpdateState()
    {

        switch (m_CurrentState)
        {
            case EnemyState.MOVETOPLAYER:

                break;

            case EnemyState.WAITING:

                break;

            case EnemyState.ATTACK:

                break;

            default:
                break;
        }
    }

    private void ExitState()
    {
        switch (m_CurrentState)
        {
            case EnemyState.MOVETOPLAYER:
                StopAllCoroutines();
                break;

            case EnemyState.WAITING:

                break;

            case EnemyState.ATTACK:

                break;

            default:
                break;
        }
    }



    private void Awake()
    {
        m_HealthController = GetComponent<HealthController>();

        m_Cells = new List<Vector3Int>();
        m_Origin = m_Tilemap.WorldToCell(this.transform.position);
        InitState(EnemyState.WAITING);

        m_HealthController.incializarHP(50);
        m_HealthController.incializarDefensa(6);
        m_HealthController.incializarDefensaMagica(4);

    }

    private void Update()
    {
        UpdateState();
    }
    
    public void Die()
    {
        if (EnemyState.WAITING != m_CurrentState)
        {
            Debug.Log("acabe");
            m_GameEventAcabeMiTurno.Raise();
        }
        
    }
    IEnumerator Move()
    {
        int i = 0;
        for(Movement = MaxMovement; Movement > 0 && !InRange(); Movement--)
        {
            yield return new WaitForSeconds(timeMoving);
            if(i < m_Cells.Count)
            {
                if (RoomManager.IsOccupied(m_Cells[i]))
                {
                    Pathfinding.FindPath(m_Origin, m_Destiny, out m_Cells, m_Tilemap);
                    i = 0;
                }
                Vector3 targetWorldPos = m_Tilemap.GetCellCenterWorld(m_Cells[i]);
                transform.position = targetWorldPos;
                m_Origin = m_Tilemap.WorldToCell(this.transform.position);
                i++;
            }
        }
        if (!InRange())
        {
            PlayerTurn();
        } else
        {
            ChangeState(EnemyState.ATTACK);
        }
        
    }
    private bool InRange()
    {
        return currentAttack.Range >= Mathf.Abs(m_Origin.x - m_Destiny.x) + Mathf.Abs(m_Origin.y - m_Destiny.y);
    }
    public void MyTurn()
    {
        Debug.Log("Turno enemigo");
        m_Origin = m_Tilemap.WorldToCell(this.transform.position);
        m_GameEventMeApunto.Raise();
        if (RoomManager.players.Length > 1)
        {
            Vector3Int playerpos1 = m_Tilemap.WorldToCell(RoomManager.players[0].transform.position);
            Vector3Int playerpos2 = m_Tilemap.WorldToCell(RoomManager.players[1].transform.position);
            if (Pathfinding.ManhattanDistance(playerpos1, m_Origin) < Pathfinding.ManhattanDistance(playerpos1, m_Origin))
            {
                m_Destiny = playerpos1;
            }
            else
            {
                m_Destiny = playerpos2;
            }
        } else
        {
            m_Destiny = m_Tilemap.WorldToCell(RoomManager.players[0].transform.position);
        }
        currentAttack = this.AttackList[Random.Range(0, AttackList.Length)];

        ChangeState(EnemyState.MOVETOPLAYER);
    }

    private void PlayerTurn()
    {
        m_GameEventAcabeMiTurno.Raise();
        ChangeState(EnemyState.WAITING);
    }

    public void InitiateSO(Enemy enemySO, Vector3Int newcell)
    {
        gameObject.GetComponent<SpriteRenderer>().sprite = enemySO.SpritE;
        this.Attack = enemySO.Attack;
        this.Magic = enemySO.Magic;
        this.Defense = enemySO.Defense;
        this.MagicDefense = enemySO.Magicdefense;
        this.Movement = enemySO.Movement;
        this.MaxMovement = enemySO.Movement;
        this.HealthPoints = enemySO.HealthPoints;
        this.HealthPointsMax = enemySO.HealthPointsMax;
        this.AttackList = enemySO.AttackList;
        this.Effect = enemySO.Effect;

        this.m_Origin = newcell;
    }

    private void OnEnable()
    {
        gameObject.GetComponent<SpriteRenderer>().sprite = enemySO.SpritE;
        this.Attack = enemySO.Attack;
        this.Magic = enemySO.Magic;
        this.Defense = enemySO.Defense;
        this.MagicDefense = enemySO.Magicdefense;
        this.Movement = enemySO.Movement;
        this.MaxMovement = enemySO.Movement;
        this.HealthPoints = enemySO.HealthPoints;
        this.HealthPointsMax = enemySO.HealthPointsMax;
        this.AttackList = enemySO.AttackList;
        this.Effect = enemySO.Effect;

        m_Origin = m_Tilemap.WorldToCell(this.transform.position);
    }

}
