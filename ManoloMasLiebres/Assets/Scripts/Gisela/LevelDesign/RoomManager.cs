using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.InputSystem;
using Unity.VisualScripting.Antlr3.Runtime.Collections;

//Es que sino no deja hacer el merge bro

public class RoomManager : MonoBehaviour
{
    [Header("Musica")]
    [SerializeField]
    private AudioClip battleMusic;
    [SerializeField]
    private AudioClip exploreMusic;
    [Header("Cosa rooms")]
    [SerializeField]
    List<Tilemap> rooms;
    [SerializeField]
    private Tilemap initRoom;
    public static Tilemap currentRoom;
    [Header("Cosa spawner")]
    [SerializeField]
    private GameObject itempool;
    [SerializeField]
    private GameObject equipablepool;
    [SerializeField]
    private Item[] items;
    [SerializeField]
    private Equipable[] equipables;
    [Header("Info room objects")]
    //[SerializeField]
    //private List<GameObject> players;
    public static PlayerController[] players;
    public static EnemyController[] enemies;
    private void Awake()
    {
        /*foreach(PlayerController player in FindObjectsByType<PlayerController>(FindObjectsInactive.Include, FindObjectsSortMode.None)) 
        {
            player.enabled = true;
        }*/
        
    }
    // Start is called before the first frame update
    void Start()
    {
        gameObject.GetComponent<AudioSource>().clip = exploreMusic;
        gameObject.GetComponent<AudioSource>().Play();
        players = FindObjectsByType<PlayerController>(FindObjectsSortMode.None);
        enemies = FindObjectsByType<EnemyController>(FindObjectsSortMode.None);
        RoomManager.currentRoom = initRoom;
        RoomManager.currentRoom.gameObject.SetActive(true);
        /*foreach (PlayerController player in FindObjectsByType<PlayerController>(FindObjectsInactive.Include, FindObjectsSortMode.None))
        {
            player.enabled = true;
        }*/
    }

    //Player1: -7.5 2.5
    //Player2: -7.5 -2.5
    public void InitNewRoom()
    {
        gameObject.GetComponent<AudioSource>().clip = battleMusic;
        gameObject.GetComponent<AudioSource>().Play();
        RoomManager.currentRoom.gameObject.SetActive(false);
        RoomManager.currentRoom = this.rooms[Random.Range(0, this.rooms.Count)];
        RoomManager.currentRoom.gameObject.SetActive(true);
        foreach (EnemyController enemy in currentRoom.transform.GetComponentsInChildren<EnemyController>(true))
        {
            enemy.gameObject.SetActive(true);
        }

        itempool.GetComponent<ObjectPool>().CleanPool();
        equipablepool.GetComponent<ObjectPool>().CleanPool();
        enemies = FindObjectsByType<EnemyController>(FindObjectsSortMode.None);
    }

    public void LoadRoom(GameObject room)
    {
        gameObject.GetComponent<AudioSource>().clip = exploreMusic;
        gameObject.GetComponent<AudioSource>().Play();
        RoomManager.currentRoom.gameObject.SetActive(false);
        RoomManager.currentRoom = room.GetComponent<Tilemap>();
        room.SetActive(true);
        foreach (EnemyController enemy in room.transform.GetComponentsInChildren<EnemyController>(true))
        {
            enemy.gameObject.SetActive(false);
        }

        itempool.GetComponent<ObjectPool>().CleanPool();
        equipablepool.GetComponent<ObjectPool>().CleanPool();
    }

    //8, 4
    public void SpawnObject()
    {
        gameObject.GetComponent<AudioSource>().clip = exploreMusic;
        gameObject.GetComponent<AudioSource>().Play();
        Vector3Int objposincel;

        do
        {
            objposincel = new Vector3Int(Random.Range(-8, 9), Random.Range(-4, 5), 0);
        } while (!Pathfinding.IsWalkableTile(objposincel, currentRoom) || IsOccupied(objposincel));
       

        Vector3 objpos = currentRoom.GetCellCenterWorld(objposincel);
        
       
        int r = Random.Range(0, 2);
        if (r == 0)
        {
            GameObject obj = itempool.GetComponent<ObjectPool>().GetPooledObject();
            r = Random.Range(0, items.Length);
            obj.GetComponent<ItemWorld>().Inicia(items[r], currentRoom, objpos);
        }
        else
        {
            GameObject obj = equipablepool.GetComponent<ObjectPool>().GetPooledObject();
            r = Random.Range(0, equipables.Length);
            obj.GetComponent<EquipWorld>().Inicia(equipables[r], currentRoom, objpos);
        }

    }
    public static bool IsOccupied(Vector3Int pos)
    {
        Vector3 raycastorigin = currentRoom.GetCellCenterWorld(pos);
        raycastorigin.z = -10;
        RaycastHit2D hit = Physics2D.Raycast(raycastorigin, Vector3.forward, 30);
        return hit.rigidbody != null;
    }
    public static bool SelectObjectsInRangedAttack(Vector3Int initposition, int range, Tilemap tiles, out List<GameObject> hitObjects)
    {
        hitObjects = new List<GameObject>();
        Vector3Int pos;
        for (int i = 0; i < players.Length; i++)
        {
            pos = tiles.WorldToCell(players[i].transform.position);
            if (Pathfinding.InManhattanDistance(pos, initposition, range))
            {
                hitObjects.Add(players[i].gameObject);
            }
        }
        for (int i = 0; i < enemies.Length; i++)
        {
            pos = tiles.WorldToCell(enemies[i].transform.position);
            if (Pathfinding.InManhattanDistance(pos, initposition, range))
            {
                hitObjects.Add(enemies[i].gameObject);
            }
        }
        return hitObjects.Count > 0;  
    }

    
    
}
