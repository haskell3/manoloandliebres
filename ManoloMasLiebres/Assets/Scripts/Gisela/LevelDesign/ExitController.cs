using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitController : MonoBehaviour
{
    [SerializeField]
    private GameEvent PlayerExit;
    [SerializeField]
    private GameEvent IniciCombat;
    private void OnTriggerEnter2D(Collider2D collision)
    {
       Debug.Log("Me voy de la sala");
       PlayerExit.Raise();
       IniciCombat.Raise();
    }

    public void ActivateExit()
    {
        this.gameObject.SetActive(true);
    }
}
