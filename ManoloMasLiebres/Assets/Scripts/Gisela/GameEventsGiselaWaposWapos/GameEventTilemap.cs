using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

[CreateAssetMenu]
public class GameEventTilemap : ScriptableObject
{
    private readonly List<GameEventListenerTilemap> eventListeners =
        new List<GameEventListenerTilemap>();

    public void Raise(Tilemap a)
    {
        for (int i = eventListeners.Count - 1; i >= 0; i--)
            eventListeners[i].OnEventRaised(a);
    }

    public void RegisterListener(GameEventListenerTilemap listener)
    {
        if (!eventListeners.Contains(listener))
            eventListeners.Add(listener);
    }

    public void UnregisterListener(GameEventListenerTilemap listener)
    {
        if (eventListeners.Contains(listener))
            eventListeners.Remove(listener);
    }
}
