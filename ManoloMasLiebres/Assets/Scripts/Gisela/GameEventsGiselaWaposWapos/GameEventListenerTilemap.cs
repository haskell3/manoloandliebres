using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Tilemaps;

public class GameEventListenerTilemap : MonoBehaviour
{
    [Tooltip("Event to register with.")]
    public GameEventTilemap Event;

    [Tooltip("Response to invoke when Event is raised.")]
    public UnityEvent<Tilemap> Response;

    private void OnEnable()
    {
        Event.RegisterListener(this);
    }

    private void OnDisable()
    {
        Event.UnregisterListener(this);
    }

    public void OnEventRaised(Tilemap a)
    {

        Response.Invoke(a);
    }
}
