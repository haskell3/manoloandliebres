using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Player", menuName = "Player/Player")]
public class Players : ScriptableObject
{

    [SerializeField]
    private Sprite spritE;
    public Sprite SpritE
    {
        get
        {
            return spritE;
        }
        set
        {
            spritE = value;
        }
    }

    [SerializeField]
    private float attack;
    public float Attack
    {
        get
        {
            return attack;
        }
        set
        {
           attack = value;
        }
    }

    [SerializeField]
    private float magic;
    public float Magic
    {
        get
        {
            return magic;
        }
        set
        {
            magic = value;
        }
    }
    [SerializeField]
    private float defense;
    public float Defense
    {
        get
        {
            return defense;
        }
        set
        {
            defense = value;
        }
    }
    [SerializeField]
    private float magicdefense;
    public float Magicdefense
    {
        get
        {
            return magicdefense;
        }
        set
        {
            magicdefense = value;
        }
    }
    [SerializeField]
    private int movement;
    public int Movement
    {
        get
        {
            return movement;
        }
        set
        {
            movement = value;
        }
    }
    [SerializeField]
    private float healthPoints;
    public float HealthPoints
    {
        get
        {
            return healthPoints;
        }
        set
        {
            healthPoints = value;
        }
    }
    [SerializeField]
    private float healthPointsMax;
    public float HealthPointsMax
    {
        get
        {
            return healthPointsMax;
        }
        set
        {
            healthPointsMax = value;
        }
    }
    [SerializeField]
    private Backpack backpack;
    public Backpack Backpack
    {
        get
        {
            return backpack;
        }
        set
        {
            backpack = value;
        }
    }
    [SerializeField]
    private Equipable armor;
    public Equipable Armor
    {
        get
        {
            return armor;
        }
        set
        {
            armor = value;
        }
    }
    [SerializeField]
    private Equipable weapon;
    public Equipable Weapon
    {
        get
        {
            return weapon;
        }
        set
        {
            weapon = value;
        }
    }
    [SerializeField]
    private Attack[] attackList;

    public Attack[] AttackList
    {
        get
        {
            return attackList;
        }
        set
        {
            attackList = value;
        }
    }

    [SerializeField]
    private Attack.Effects effect;
    public Attack.Effects Effect
    {
        get
        {
            return effect;
        }
        set
        {
            effect = value;
        }
    }

    [SerializeField]
    private Transform position;
    public Transform Position
    {
        get
        {
            return position;
        }
        set
        {
            position = value;
        }
    }
}
