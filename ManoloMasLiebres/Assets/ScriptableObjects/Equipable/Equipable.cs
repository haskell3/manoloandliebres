using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Equipable", menuName = "Player/Equipable")]
public class Equipable : ScriptableObject, ItemInterface
{
    public enum TypeEquipable { ARMOR, WEAPON};

    [SerializeField]
    private string name;
    public string Name
    {
        get
        {
            return name;
        }
        set
        {
            name = value;
        }
    }

    [SerializeField]
    private Sprite spritE;
    public Sprite SpritE
    {
        get
        {
            return spritE;
        }
        set
        {
            spritE = value;
        }
    }

    [SerializeField]
    private float attack;
    public float Attack
    {
        get
        {
            return attack;
        }
        set
        {
            attack = value;
        }
    }

    [SerializeField]
    private float magic;
    public float Magic
    {
        get
        {
            return magic;
        }
        set
        {
            magic = value;
        }
    }
    [SerializeField]
    private float defense;
    public float Defense
    {
        get
        {
            return defense;
        }
        set
        {
            defense = value;
        }
    }
    [SerializeField]
    private float magicdefense;
    public float Magicdefense
    {
        get
        {
            return magicdefense;
        }
        set
        {
            magicdefense = value;
        }
    }
    [SerializeField]
    private TypeEquipable typeEquip;
    public TypeEquipable TypeEquip
    {
        get
        {
            return typeEquip;
        }
        set
        {
            typeEquip = value;
        }

    }
}
