using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Attack", menuName = "Player/Attack")]
public class Attack : ScriptableObject
{
    public enum Effects { Effect1, Effect2, Effect3 };
    public enum TypeAttack { Pooled, NoPooled, Impulsive };
    public enum TypeDamage { ATTACK, MAGIC };

    [SerializeField]
    private string name;
    public string Name
    {
        get
        {
            return name;
        }
        set
        {
            name = value;
        }
    }

    [SerializeField]
    private int range;
    public int Range
    {
        get
        {
            return range;
        }
        set
        {
            range = value;
        }
    }

    [SerializeField]
    private int area;
    public int Area
    {
        get
        {
            return area;
        }
        set
        {
            area = value;
        }
    }

    [SerializeField]
    private float damage;
    public float Damage
    {
        get
        {
            return damage;
        }
        set
        {
            damage = value;
        }
    }

    [SerializeField]
    private int cooldown;
    public int Cooldown
    {
        get
        {
            return cooldown;
        }
        set
        {
            cooldown = value;
        }
    }

    [SerializeField]
    private int turnosCooldown;
    public int TurnosCooldown
    {
        get
        {
            return turnosCooldown;
        }
        set
        {
            turnosCooldown = value;
        }
    }

    [SerializeField]
    private TypeDamage typeDmg;
    public TypeDamage TypeDmg
    {
        get
        {
            return typeDmg;
        }
        set
        {
            typeDmg = value;
        }
    }

    [SerializeField]
    private Effects effect;
    public Effects Effect
    {
        get
        {
            return effect;
        }
        set
        {
            effect = value;
        }
    }
    [SerializeField]
    private TypeAttack typeAtk;
    public TypeAttack TypeAtk
    {
        get
        {
            return typeAtk;
        }
        set
        {
            typeAtk = value;
        }
    }
}
