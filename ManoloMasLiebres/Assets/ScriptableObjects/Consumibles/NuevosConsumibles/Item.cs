using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public abstract class Item : ScriptableObject, IItem
{
    [Header("Common fields")]
    [SerializeField]
    private string name = "";
    public string Name { get => name; set => name = value; }
    [SerializeField]
    private Sprite m_Sprite = null;
    public Sprite Sprite { get => m_Sprite; set => m_Sprite = value; }

    public abstract bool UsedBy(GameObject go);
}

