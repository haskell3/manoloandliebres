using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Attack Item", menuName = "Items/Attack")]
public class AttackItem : Item
{
    [Header("Damaging Item values")]
    [SerializeField]
    [Min(0f)]
    private float m_Attack;

    public override bool UsedBy(GameObject go)
    {
        if (!go.TryGetComponent(out IAttack attackItem))
            return false;

        attackItem.AttackPotion(m_Attack);
        return true;
    }
}
