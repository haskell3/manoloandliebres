using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Damaging Item", menuName = "Items/Damaging")]
public class DamagingItem : Item
{
    [Header("Damaging Item values")]
    [SerializeField]
    [Min(0f)]
    private float m_Damage;

    public override bool UsedBy(GameObject go)
    {
        if (!go.TryGetComponent(out IDamageable damageable))
            return false;

        damageable.Damage(10, m_Damage, 50, Attack.TypeDamage.MAGIC);
        return true;
    }
}
