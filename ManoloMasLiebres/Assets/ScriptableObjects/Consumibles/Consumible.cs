using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Old Item", menuName = "Items/Old Item")]
public class Consumible : ScriptableObject
{
    [SerializeField]
    private string name;
    public string Name
    {
        get
        {
            return name;
        }
        set
        {
            name = value;
        }
    }

    [SerializeField]
    private Sprite spritE;
    public Sprite SpritE
    {
        get
        {
            return spritE;
        }
        set
        {
            spritE = value;
        }
    }

    [SerializeField]
    private float healthPoints;
    public float HealthPoints
    {
        get
        {
            return healthPoints;
        }
        set
        {
            healthPoints = value;
        }
    }
    [SerializeField]
    private int movement;
    public int Movement
    {
        get
        {
            return movement;
        }
        set
        {
            movement = value;
        }
    }
}
